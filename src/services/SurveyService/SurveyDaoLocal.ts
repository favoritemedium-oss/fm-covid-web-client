import {
  ISurveyDao,
  Gender,
  Preconditions,
  IUserStats
} from './SurveyDao';

const AGE_STORE_KEY = 'age';
const GENDER_STORE_KEY = 'gender';
const PRECONDITIONS_STORE_KEY = 'preconditions';

const ZERO_LENGTH = 0;

// Dao implementation in case if we would want to store data only in user's localStore.
export default class SurveyDaoLocal implements ISurveyDao {
  public async setAge(age: number): Promise<void> {
    localStorage.setItem(AGE_STORE_KEY, age.toString());
  }

  public async getAge(): Promise<number|null> {
    const ageStr = localStorage.getItem(AGE_STORE_KEY);
    const age = Number(ageStr);
    if (!ageStr || Number.isNaN(age)) {
      return null;
    }

    return age;
  }

  public async setGender(gender: Gender): Promise<void> {
    localStorage.setItem(GENDER_STORE_KEY, gender.toString());
  }

  public async getGender(): Promise<Gender|null> {
    const genderStr = localStorage.getItem(GENDER_STORE_KEY);
    const genders = Object.values(Gender) as string[];

    if (!genderStr || !genders.includes(genderStr)) {
      return null;
    }

    return genderStr as Gender;
  }

  public async setPreconditions(preconditions: Preconditions[]): Promise<void> {
    localStorage.setItem(PRECONDITIONS_STORE_KEY, preconditions.toString());
  }

  public async getPreconditions(): Promise<Preconditions[]|null> {
    const preconditionsStr = localStorage.getItem(PRECONDITIONS_STORE_KEY);

    if (!preconditionsStr) {
      return null;
    }

    const preconditionsArray = preconditionsStr.split(',');
    if (preconditionsArray.length === ZERO_LENGTH) {
      return [];
    }

    const preconditions: Preconditions[] = [];
    const preconditionValues = Object.values(Preconditions) as string[];
    for (const precondition of preconditionsArray) {
      if (!preconditionValues.includes(precondition)) {
        await this.setPreconditions([]);
        return null;
      }

      preconditions.push(precondition as Preconditions);
    }

    return preconditions;
  }

  public async loadUserStats(): Promise<IUserStats|null> {
    const [age, gender, preconditions] = await Promise.all([
      this.getAge(),
      this.getGender(),
      this.getPreconditions()
    ]);

    return { age, gender, preconditions };
  }

  public async resetData(): Promise<void> {
    localStorage.removeItem(AGE_STORE_KEY);
    localStorage.removeItem(GENDER_STORE_KEY);
    localStorage.removeItem(PRECONDITIONS_STORE_KEY);
  }
}
