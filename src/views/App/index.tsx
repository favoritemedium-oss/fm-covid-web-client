import React, {
  ReactElement, useEffect, useState
} from 'react';
import {
  Switch,
  Route,
  BrowserRouter,
  Link
} from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { ThemeProvider } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';

import Home from '../../pages/Home';
import NotFound from '../../pages/NotFound';
import { useSurveyService } from '../../services/SurveyService';
import theme from '../../theme';
import AppHeader from '../AppHeader';
import Assessment from '../../pages/Assessment';
import { useStatisticService } from '../../services/StatisticService';
import PreventionTips from '../../pages/PreventionTips';
import { useStyles } from './styles';
import AboutData from '../../pages/AboutData';
import Disclaimer from '../../pages/Disclaimer';

const App = observer((): ReactElement => {
  const surveyService = useSurveyService();
  const statisticService = useStatisticService();
  const [loaded, setLoaded] = useState(false);
  const { t } = useTranslation();
  const styles = useStyles();

  useEffect(() => {
    const load = async (): Promise<void> => {
      await Promise.all([
        surveyService.load(),
        statisticService.load()
      ]);
      setLoaded(true);
    };
    load();
  }, [surveyService, statisticService, loaded]);

  const renderPages = (): ReactElement => (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/assessment">
        <Assessment />
      </Route>
      <Route path="/prevention-tips">
        <PreventionTips />
      </Route>
      <Route path="/about-data">
        <AboutData />
      </Route>
      <Route path="/disclaimer">
        <Disclaimer />
      </Route>
      <Route path="*">
        <NotFound />
      </Route>
    </Switch>
  );

  const renderFooter = (): ReactElement => (
    <div className={styles.footer}>
      <small>
        {`${t('MadeIn')} `}
        <a
          className={styles.fmLink}
          href="https://www.favoritemedium.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Favorite Medium
        </a>
      </small>
      <p>
        <small><Link to="/about-data">{t('AboutData')}</Link></small>
        |
        <small><Link to="/disclaimer">{t('DisclaimerTitle')}</Link></small>
      </p>
    </div>
  );

  const renderApp = (): ReactElement => (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <AppHeader />
        <div className={styles.app}>
          {renderPages()}
          {renderFooter()}
        </div>
      </BrowserRouter>
    </ThemeProvider>
  );

  const renderLoader = (): ReactElement => (
    <div className={styles.loading}>Loading...</div>
  );

  return loaded ? renderApp() : renderLoader();
});

export default App;
