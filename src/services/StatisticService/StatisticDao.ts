import { Preconditions } from '../SurveyService/SurveyDao';

export interface IGroupStatistic {
  cfrKnown: number;
  patientCountKnown: number;
  patientCountEstimated: number;
  cfrEstimated: number;
  totalPopulation: number;
  percentageInfected: number;
  mortalityPer100k: number;
}

export interface IMortalityStats {
  [name: string]: number
}

type IComorbidity = {
  name: Preconditions;
  value: number;
};

export interface IGenderStatistic {
  female: IGroupStatistic;
  male: IGroupStatistic;
  mortalityStats: IMortalityStats;
  comorbidity: IComorbidity[];
}

export interface IStatistic {
  groupsStatistics: IGroupStatistic[];
  statisticByAge: IGenderStatistic[];
}

export interface IStatisticDao {
  loadStatistic(): Promise<IStatistic | null>;
}
