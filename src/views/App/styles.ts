import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
  app: {
    backgroundColor: '#fff',
    flex: 1,
    minHeight: '100%',
    display: 'flex',

    flexDirection: 'column',
    [theme.breakpoints.up('md')]: {
      backgroundColor: '#EBEEF2'
    }
  },

  loading: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    color: '#E6447E'
  },

  footer: {
    textAlign: 'center',
    padding: '15px 15px',
    color: '#666',
    fontFamily: 'Open Sans, sans-serif',
    fontSize: '13px',

    '& a': {
      textDecoration: 'none',
      color: '#666',
      whiteSpace: 'nowrap',

      '&:hover': {
        textDecoration: 'underline'
      }
    },

    '& p': {
      textDecoration: 'none',
      color: '#666',
      marginBottom: 0
    }
  },

  fmLink: {
    fontWeight: 'bold'
  }
}));
