import React, { ReactElement } from 'react';
import Divider from '@material-ui/core/Divider';
import dayjs from 'dayjs';
import { useTranslation } from 'react-i18next';
import { toJS } from 'mobx';

import { useStyles } from './styles';
import { useStatisticService } from '../../services/StatisticService';
import { useSurveyService } from '../../services/SurveyService';
import { Gender, Preconditions } from '../../services/SurveyService/SurveyDao';
import SurveyContainer from '../../components/SurveyContainer';
import Button from '../../components/Button';
import Chart from '../../components/Chart';
import Bar from '../../components/Bar';

interface IMortality {
  rate: number;
  name: string;
  color: string;
}

interface ICondition {
  name: string;
  value: number;
}

interface IProps {
  onRestart: () => void;
}

const chartColors = ['#68B881', '#76CBC8', '#75849A', '#B6DCC2', '#FBC877', '#E6447D'];

function Result({ onRestart }: IProps): ReactElement {
  const statisticService = useStatisticService();
  const surveyService = useSurveyService();
  const styles = useStyles();
  const { t } = useTranslation();
  const ageGap = 9;
  const { age, gender, preconditions } = surveyService;
  const estimatedaAge = age !== null ? age : 0;

  let survivability = 100;

  const { statByAge } = statisticService;
  const data = gender === Gender.Female ? statByAge[estimatedaAge].female : statByAge[estimatedaAge].male;
  const { cfrEstimated, percentageInfected, mortalityPer100k } = data;
  const estimate = cfrEstimated;
  const infectionRate = percentageInfected;
  const { mortalityStats, comorbidity } = statByAge[estimatedaAge];
  const death = +mortalityPer100k.toFixed(1);
  const mortalityStatsArray: IMortality[] = [{ name: 'COVID-19', rate: death, color: '#E6447D' }];

  Object.keys(mortalityStats).forEach((val, i): void => {
    if (mortalityStats[val] > 1) {
      mortalityStatsArray.push({ rate: +mortalityStats[val].toFixed(1), name: val, color: chartColors[i] });
    }
  });

  mortalityStatsArray.sort((a, b) => ((a.rate < b.rate) ? 1 : -1));

  const comorbidityArray = toJS(comorbidity);
  comorbidityArray.sort((a, b) => ((a.value < b.value) ? 1 : -1));

  const allConditionsWithValue: ICondition[] = [];

  comorbidityArray.forEach((condition) => {
    if (preconditions?.includes(condition.name)) {
      allConditionsWithValue.push({ name: condition.name, value: condition.value });
    }
  });

  if (allConditionsWithValue.length > 0) {
    survivability = 100 - (estimate * allConditionsWithValue[0].value);
  } else {
    survivability = 100 - estimate;
  }

  const currentDate = `${dayjs().utc().format('YYYY-MM-DD')}`;

  // Get translated condition
  const getConditions = (): string => {
    const { None } = Preconditions;
    const conditions: string[] = [];

    preconditions?.forEach((condition) => {
      switch (condition) {
        case Preconditions.Cancer:
          conditions.push(t('Cancer'));
          break;
        case Preconditions.CardiovascularDisease:
          conditions.push(t('CardiovascularDisease'));
          break;
        case Preconditions.ChronicRespiratoryDisease:
          conditions.push(t('ChronicRespiratoryDisease'));
          break;
        case Preconditions.Diabetes:
          conditions.push(t('Diabetes'));
          break;
        case Preconditions.Hypertension:
          conditions.push(t('Hypertension'));
          break;
        default:
          break;
      }
    });

    return preconditions?.includes(None) || preconditions === null
      ? ` ${t('WithNoPrecondition')}` : ` ${t('WithPreExisting')} ${conditions?.join(', ')}`;
  };

  const getGender = (): string => (gender === Gender.Male ? t('Male') : t('Female'));

  const renderChartBar = (index: number): ReactElement => {
    const stat = mortalityStatsArray[index];

    return (
      <Bar
        key={stat.name}
        label={stat.name}
        value={stat.rate}
        color={stat.color}
        labelColor={stat.name === 'COVID-19' ? '#E6447D' : '#75849A'}
        maxValue={mortalityStatsArray[0].rate}
        valueLabel={(value) => (value ? String(value) : t('ZeroDeaths'))}
      />
    );
  };

  const renderRate = (rate: number, title: string, notes: string, cls: string): ReactElement => (
    <div className={cls}>
      <p className={styles.title}>
        {title}
        <span>{notes}</span>
      </p>
      <p className="rate">
        {`${rate.toFixed(2)}`}
        <span>%</span>
      </p>
    </div>
  );

  return (
    <SurveyContainer>
      <p className={styles.highlight}>
        {` ${t('SouthKorean')} ${getGender()}, ${estimatedaAge - ageGap}-${estimatedaAge} ${t('YearsOld')}`}
        {getConditions()}
      </p>
      <div className={styles.rates}>
        {renderRate(infectionRate, t('InfectionRate'), '*', styles.infection)}
        {renderRate(survivability, t('CaseSurvivalRate'), '**', styles.survival)}
      </div>
      <Chart
        title={t('ComparisonDeath')}
        subtitle={t('PerPeople')}
        count={mortalityStatsArray.length}
        renderBar={renderChartBar}
      />
      <Divider />
      <div className={styles.bottomSection}>
        <Button className={styles.button} onClick={onRestart}>{t('Repeat')}</Button>
        <div className={styles.data}>
          <small>
            <span>*</span>
            {`${t('PercentagePeople')} ${currentDate}.`}
          </small>
          <small>
            <span>**</span>
            {`${t('PercentageSurvival')} ${currentDate}.`}
          </small>
        </div>

        <div className={styles.prevention}>
          <p>{t('LearnText')}</p>
          <Button className={styles.button} to="/prevention-tips/">{t('PreventionTips')}</Button>
        </div>

      </div>
    </SurveyContainer>
  );
}

export default Result;
