import State from './State';

export default class FailureState<T = null> extends State<T> {
  constructor(public readonly error: Error, value: T|State<T>|null = null) {
    super(value);
  }
}
