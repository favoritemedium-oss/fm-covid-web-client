import React, { ReactElement, Suspense } from 'react';
import ReactDOM from 'react-dom';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import './index.css';
import App from './views/App';
import { SurveyServiceProvider } from './services/SurveyService';
import SurveyDaoLocal from './services/SurveyService/SurveyDaoLocal';
import { StatisticServiceProvider } from './services/StatisticService';
import StatisticDaoJson from './services/StatisticService/StatisticDaoJson';

import * as serviceWorker from './serviceWorker';
import './i18n';

dayjs.extend(utc);

const renderApp = (): ReactElement => (
  <SurveyServiceProvider dao={new SurveyDaoLocal()}>
    <StatisticServiceProvider dao={new StatisticDaoJson()}>
      <Suspense fallback={null}>
        <App />
      </Suspense>
    </StatisticServiceProvider>
  </SurveyServiceProvider>
);

ReactDOM.render(renderApp(), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
