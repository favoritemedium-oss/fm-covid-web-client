import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => createStyles({
  highlight: {
    backgroundColor: '#EBEEF2',
    color: '#373C58',
    borderRadius: '10px',
    padding: '10px 5px',
    fontFamily: 'Montserrat, sans-serif'
  },

  title: {
    marginTop: '0',
    fontSize: '12px',

    '& span': {
      color: '#E6447E',
      fontFamily: 'Open Sans, sans-serif'
    }
  },

  rates: {
    display: 'flex',
    flexDirection: 'row',

    '& > div': {
      flexGrow: '1',
      flexBasis: '0'
    },

    '& .rate': {
      fontFamily: 'Montserrat, sans-serif',
      fontSize: '50px',
      fontWeight: 'bold',
      '& span': {
        fontSize: '20px'
      }
    }
  },

  infection: {
    borderRight: '1px solid #C4C4C4',

    '& .rate': {
      color: '#373C58',
      margin: '0'
    }
  },

  survival: {
    '& .rate': {
      color: '#68B881',
      margin: '0'
    }
  },

  button: {
    maxWidth: '270px',
    margin: '20px 0 0',
    padding: '5px 0',
    textTransform: 'uppercase'
  },

  data: {
    '& small': {
      display: 'block',
      marginTop: '20px',
      fontWeight: 'normal',
      color: '#666',

      '& span': {
        color: '#E6447E',
        fontFamily: 'Open Sans, sans-serif'
      }
    }
  },

  bottomSection: {
    marginTop: '40px'
  },

  prevention: {
    margin: '40px 0',
    backgroundColor: '#EBEEF2',
    borderRadius: '10px',
    padding: '30px 20px',

    '& p': {
      marginTop: '0',
      color: '#E6447E'
    }
  }
}));
