import React, { ReactElement, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import i18next from 'i18next';
import FormControl from '@material-ui/core/FormControl';
import MenuIcon from '@material-ui/icons/Menu';
import {
  MenuItem,
  Select,
  Toolbar,
  AppBar,
  ThemeProvider,
  IconButton,
  Typography,
  List,
  Divider,
  Drawer
} from '@material-ui/core';

import { useStyles, headerTheme } from './styles';

const AppHeader = observer(() => {
  const styles = useStyles();
  const [lang, setLang] = React.useState('');
  const [state, setState] = React.useState({ left: false });
  const { t } = useTranslation();

  useEffect(() => {
    setLang(i18next.language);
  }, []);

  const changeLang = (event: React.ChangeEvent<{ value: unknown }>): void => {
    const selectedLanguage = event.target.value as string;
    setLang(selectedLanguage);
    i18next.changeLanguage(selectedLanguage);
  };

  const renderLangSelector = (): ReactElement => (
    <FormControl variant="outlined" className={styles.select}>
      <Select value={lang} onChange={changeLang}>
        <MenuItem value="en">En</MenuItem>
        <MenuItem value="ja">日</MenuItem>
        {/* TODO: uncomment if the language is ready */}
        {/* <MenuItem value="zh">Zh</MenuItem>
        <MenuItem value="ko">한</MenuItem> */}
      </Select>
    </FormControl>
  );

  const toggleDrawer = (open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent,
  ) => {
    if (event.type === 'keydown' && (
      (event as React.KeyboardEvent).key === 'Tab' || (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, left: open });
  };

  const renderMobileList = (): ReactElement => (
    <div
      className={styles.list}
      role="presentation"
      onClick={toggleDrawer(false)}
      onKeyDown={toggleDrawer(false)}
    >
      <List>
        <MenuItem className={styles.menuItem} component={Link} to="/prevention-tips/">{t('PreventionTips')}</MenuItem>
        <MenuItem className={styles.menuItem} component={Link} to="/about-data/">{t('AboutData')}</MenuItem>
      </List>
      <Divider />
    </div>
  );

  const renderDesktopList = (): ReactElement => (
    <div className={styles.sectionDesktop}>
      <MenuItem className={styles.menuItem} component={Link} to="/prevention-tips/">{t('PreventionTips')}</MenuItem>
      <MenuItem className={styles.menuItem} component={Link} to="/about-data/">{t('AboutData')}</MenuItem>
    </div>
  );

  return (
    <ThemeProvider theme={headerTheme}>
      <AppBar elevation={1}>
        <Toolbar>
          <IconButton
            className={styles.hamburger}
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={toggleDrawer(true)}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            className={styles.typography}
            color="inherit"
            component={Link}
            to="/"
          >
            {t('COVID19Outcomes')}
          </Typography>
          <div className={styles.spacer} />
          {renderDesktopList()}
          {renderLangSelector()}
        </Toolbar>
      </AppBar>
      <Drawer open={state.left} onClose={toggleDrawer(false)}>
        {renderMobileList()}
      </Drawer>
    </ThemeProvider>
  );
});

export default AppHeader;
