import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';
import MaterialButton from '@material-ui/core/Button';
import clsx from 'clsx';

import { useStyles } from './styles';

interface IProps {
  to?: string;
  secondary?: boolean;
  onClick?: () => void;
  className?: string;
  children: string;
}

function Button(props: IProps): ReactElement {
  const {
    to,
    children,
    secondary,
    onClick,
    className
  } = props;

  const styles = useStyles();
  const cls = clsx(styles.button, className, { [styles.primary]: !secondary });
  const variant = secondary ? 'outlined' : 'contained';

  if (to && onClick) {
    throw new Error('"to" and "onClick" props cannot be provided at the same time');
  }

  if (!to) {
    return <MaterialButton className={cls} variant={variant} onClick={onClick}>{children}</MaterialButton>;
  }

  return (
    <MaterialButton
      className={cls}
      component={Link}
      to={to}
      variant={variant}
    >
      {children}
    </MaterialButton>
  );
}

export default Button;
