import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import FMImage from '../../assets/images/favorite_medium_checklist.svg';
import { useStyles } from './styles';
import PageContainer from '../../components/PageContainer';
import Button from '../../components/Button';

function Home(): ReactElement {
  const styles = useStyles();
  const { t } = useTranslation();

  return (
    <PageContainer>
      <div className={styles.innerStyle}>
        <img src={FMImage} className={styles.imageStyle} alt="" />
        <h1 className={styles.header}>{t('CheckoutTitle')}</h1>
        <p className={styles.paragraph}>
          {t('CheckoutSubtitle')}
        </p>
        <Button to="/assessment/">{t('Start')}</Button>
      </div>
    </PageContainer>
  );
}

export default Home;
