import React, { ReactElement, useEffect } from 'react';
import { Button, FormHelperText } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import clsx from 'clsx';

import { useStyles } from './styles';
import FMMaleImage from '../../assets/images/favorite_medium_male.svg';
import FMFemaleImage from '../../assets/images/favorite_medium_female.svg';
import { useSurveyService } from '../../services/SurveyService';
import { Gender } from '../../services/SurveyService/SurveyDao';
import SurveyContainer from '../../components/SurveyContainer';

interface IProp {
  hasError: boolean;
}

function GenderAssessment({ hasError }: IProp): ReactElement {
  const surveyService = useSurveyService();
  const [gender, setGender] = React.useState('');
  const styles = useStyles();
  const { t } = useTranslation();

  const selectGender = (genderSelected: Gender): void => {
    setGender(genderSelected);
    surveyService.setGender(genderSelected);
  };

  useEffect(() => {
    if (surveyService.gender) {
      setGender(surveyService.gender);
    }
  }, [surveyService.gender]);

  return (
    <SurveyContainer header={`${t('Gender')}?`}>
      <div className={styles.genderSelection}>
        <div className={clsx({ active: gender === Gender.Female })}>
          <Button onClick={() => selectGender(Gender.Female)}>
            <div className={styles.buttonContent}>
              <img src={FMFemaleImage} alt={t('Female')} />
              <p>{t('Female')}</p>
            </div>
          </Button>
        </div>
        <div className={clsx({ active: gender === Gender.Male })}>
          <Button onClick={() => selectGender(Gender.Male)}>
            <div className={styles.buttonContent}>
              <img src={FMMaleImage} alt={t('Male')} />
              <p>{t('Male')}</p>
            </div>
          </Button>
        </div>
      </div>
      {hasError && <FormHelperText>{t('GenderRequired')}</FormHelperText>}
    </SurveyContainer>
  );
}

export default GenderAssessment;
