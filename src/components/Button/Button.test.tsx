import React from 'react';
import { MemoryRouter as Router } from 'react-router-dom';
import Enzyme, { mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Button from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('<Button />', () => {
  it('Should contain a link in case if has "to" property provided', () => {
    const wrapper = mount(
      <Router>
        <Button to="/">Test</Button>
      </Router>
    );

    expect(wrapper.find('a')).toHaveLength(1);
  });

  it('Should throw an error if both "to" and "onClick" properties were provided', () => {
    expect(() => shallow(<Button to="/" onClick={() => {}}>Test</Button>)).toThrowError();
  });

  it('Should have "primary" style in case if secondary was not provided', () => {
    const wrapper = mount(<Button onClick={() => {}}>Test</Button>);
    expect(wrapper.find('button').hasClass(/primary/i)).toBeTruthy();
  });

  it('Should be "outlined" style in case if "secondary" prop was provided', () => {
    const wrapper = mount(<Button secondary onClick={() => {}}>Test</Button>);
    expect(wrapper.find('button').hasClass(/outlined/i)).toBeTruthy();
  });

  it('Should be "contained" style in case if "secondary" prop was NOT provided', () => {
    const wrapper = mount(<Button onClick={() => {}}>Test</Button>);
    expect(wrapper.find('button').hasClass(/contained/i)).toBeTruthy();
  });
});
