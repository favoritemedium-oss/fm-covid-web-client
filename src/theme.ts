import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
  palette: {
    primary: {
      main: '#333333'
    },
    secondary: {
      main: '#aed581'
    }
  }
});
