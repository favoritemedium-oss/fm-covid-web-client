export default abstract class State<T = null> {
  protected val: T|null;

  public get value(): T {
    if (!this.val) {
      throw new Error('Attempt to get value from undefined state');
    }

    return this.val;
  }

  constructor(state: T|State<T>|null = null) {
    this.val = (state instanceof State) ? state.val : state;
  }
}
