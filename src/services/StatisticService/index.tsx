
import React, { useState } from 'react';
import { observable, computed, action } from 'mobx';
import { IStatisticDao, IStatistic, IGenderStatistic } from './StatisticDao';
import StatisticDaoJson from './StatisticDaoJson';
import Service, { IServiceContextProps, createServiceHook } from '../Service';

class StatisticService extends Service<IStatisticDao> {
  @observable private statistic: IStatistic|null = null;

  @computed public get statByAge(): IGenderStatistic[] {
    if (this.statistic === null) {
      throw new Error('Attempt to get unloaded statistic.');
    }

    return this.statistic.statisticByAge;
  }

  @action public async load(): Promise<void> {
    this.statistic = await this.dao.loadStatistic();
  }
}

// Create Context and service hooks
const StatisticServiceContext = React.createContext<StatisticService|null>(null);
type StatisticProps = IServiceContextProps<IStatisticDao>;
export const StatisticServiceProvider = ({ dao, children }: StatisticProps): React.ReactElement => {
  const [service] = useState(new StatisticService(dao));
  return <StatisticServiceContext.Provider value={service}>{children}</StatisticServiceContext.Provider>;
};

StatisticServiceProvider.defaultProps = {
  dao: new StatisticDaoJson()
};

export const useStatisticService = createServiceHook<StatisticService, IStatisticDao>(StatisticServiceContext);
