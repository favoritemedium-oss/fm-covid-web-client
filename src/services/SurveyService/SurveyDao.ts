export enum Gender {
  Male = 'male',
  Female = 'female'
}

export enum Preconditions {
  None = 'none',
  Cancer = 'cancer',
  CardiovascularDisease = 'cardiovascular-disease',
  ChronicRespiratoryDisease = 'chronicRespiratory-disease',
  Diabetes = 'diabetes',
  Hypertension = 'hypertension'
}

export interface IUserStats {
  age: number|null;
  gender: Gender|null;
  preconditions: Preconditions[]|null;
}

export interface ISurveyDao {
  setAge(age: Number): Promise<void>;

  getAge(): Promise<number|null>;

  setGender(gender: Gender): Promise<void>;

  getGender(): Promise<Gender|null>;

  setPreconditions(preconditions: Preconditions[]): Promise<void>;

  getPreconditions(): Promise<Preconditions[]|null>;

  loadUserStats(): Promise<IUserStats|null>;

  resetData(): Promise<void>;
}
