import {
  ISurveyDao,
  Gender,
  Preconditions,
  IUserStats
} from './SurveyDao';

// Dao implementation in case if we would want to store data only in user's localStore.
export default class SurveyDaoStub implements ISurveyDao {
  public async setAge(): Promise<void> {
    throw new Error('Not implemented');
  }

  public async getAge(): Promise<number|null> {
    throw new Error('Not implemented');
  }

  public async setGender(): Promise<void> {
    throw new Error('Not implemented');
  }

  public async getGender(): Promise<Gender|null> {
    throw new Error('Not implemented');
  }

  public async setPreconditions(): Promise<void> {
    throw new Error('Not implemented');
  }

  public async getPreconditions(): Promise<Preconditions[]|null> {
    throw new Error('Not implemented');
  }

  public async loadUserStats(): Promise<IUserStats|null> {
    throw new Error('Not implemented');
  }

  public async resetData(): Promise<void> {
    throw new Error('Not implemented');
  }
}
