import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Container from '@material-ui/core/Container';

import AgeGroup from '../../views/AgeGroup';
import Gender from '../../views/Gender';
import HealthCondition from '../../views/HealthCondition';
import Result from '../../views/Result';
import { useStyles } from './style';
import { useSurveyService } from '../../services/SurveyService';
import PageContainer from '../../components/PageContainer';
import Button from '../../components/Button';

enum Steps {
  AgeGroupStep = 0,
  GenderStep = 1,
  HealthConditionStep = 2,
  ResultStep = 3
}

const stepLabels = ['Step 1', 'Step 2', 'Step 3', 'Result'];

export default function Assessment(): ReactElement {
  const surveyService = useSurveyService();
  const { t } = useTranslation();
  const styles = useStyles();
  const [activeStep, setActiveStep] = React.useState(Steps.AgeGroupStep);
  const [error, setError] = React.useState(false);

  const handleNext = (): void => {
    if (!surveyService.age && activeStep === Steps.AgeGroupStep) {
      setError(true);
      return;
    }
    if (!surveyService.gender && activeStep === Steps.GenderStep) {
      setError(true);
      return;
    }
    setError(false);
    setActiveStep((prevActiveStep) => prevActiveStep + Steps.GenderStep);
  };

  const handleBack = (): void => {
    setActiveStep((prevActiveStep) => prevActiveStep - Steps.GenderStep);
  };

  const handleReset = (): void => {
    setActiveStep(Steps.AgeGroupStep);
    surveyService.resetData();
  };

  React.useEffect(() => {
    surveyService.resetData();
    setActiveStep(Steps.AgeGroupStep);
  }, [surveyService]);

  const renderStepper = (): ReactElement => (
    <Stepper activeStep={activeStep} alternativeLabel>
      {
        stepLabels.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))
      }
    </Stepper>
  );

  const renderStepContent = (): ReactElement | string => {
    switch (activeStep) {
      case Steps.AgeGroupStep:
        return <AgeGroup hasError={error} />;
      case Steps.GenderStep:
        return <Gender hasError={error} />;
      case Steps.HealthConditionStep:
        return <HealthCondition />;
      case Steps.ResultStep:
        return <Result onRestart={handleReset} />;
      default:
        return 'Unknown stepIndex';
    }
  };

  const renderStepButtons = (): ReactElement|null => {
    if (activeStep === Steps.ResultStep) {
      return null;
    }

    const prevButton = activeStep === Steps.AgeGroupStep
      ? <Button secondary to="/">{t('Previous')}</Button>
      : <Button secondary onClick={handleBack}>{t('Previous')}</Button>;

    return (
      <div className={styles.buttonGroup}>
        {prevButton}
        <Button onClick={handleNext}>{t('Next')}</Button>
      </div>
    );
  };

  const renderSteps = (): ReactElement => (
    <div>
      {renderStepContent()}
      {renderStepButtons()}
    </div>
  );

  const renderResult = (): ReactElement => (
    <Container>
      <h1 className={styles.heading}>{t('StepsCompleted')}</h1>
      <div className={styles.buttonGroup}>
        <Button onClick={handleReset}>{t('Repeat')}</Button>
      </div>
    </Container>
  );

  return (
    <PageContainer className={styles.content}>
      {renderStepper()}
      {activeStep < stepLabels.length ? renderSteps() : renderResult()}
    </PageContainer>
  );
}
