import React, { ReactNode, ReactElement } from 'react';

import { useStyles } from './styles';

interface IProps {
  children: ReactNode;
}

export default function Typography({ children }: IProps): ReactElement {
  const styles = useStyles();
  return <div className={styles.typography}>{children}</div>;
}
