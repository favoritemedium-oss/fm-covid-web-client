import React, { ReactNode, ReactElement } from 'react';
import Container from '@material-ui/core/Container';

import { useStyles } from './styles';

interface IProps {
  header?: string;
  children: ReactNode;
}

function SurveyContainer({ header, children }: IProps): ReactElement {
  const styles = useStyles();

  return (
    <Container className={styles.containerStyle}>
      {header && <h1 className={styles.header}>{header}</h1>}
      {children}
    </Container>
  );
}

export default SurveyContainer;
