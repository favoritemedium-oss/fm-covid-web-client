import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => createStyles({
  typography: {
    '& p': {
      fontFamily: 'Open Sans, sans-serif',

      '& a': {
        color: '#68B881',
        fontWeight: 'bold',
        textDecoration: 'none'
      }
    }
  }
}));
