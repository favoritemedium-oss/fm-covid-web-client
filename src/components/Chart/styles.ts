import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => createStyles({
  chart: {
    marginTop: '30px'
  },

  title: {
    color: '#373C58',
    fontWeight: 'bold',
    margin: '0'
  },

  subtitle: {
    color: '#373C58',
    fontSize: '12px',
    margin: '0'
  },
  chartArea: {
    margin: '10px 0',

    '& p': {
      fontSize: '10px'
    }
  }
}));
