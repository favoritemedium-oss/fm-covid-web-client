import React, { ReactElement, useEffect } from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import { useTranslation } from 'react-i18next';
import { observer } from 'mobx-react-lite';

import { useStyles } from './styles';
import { useSurveyService } from '../../services/SurveyService';
import FMCalendar from '../../assets/images/favorite_medium_calendar.svg';
import SurveyContainer from '../../components/SurveyContainer';

interface IProps {
  hasError: boolean;
}

const AgeGroup = observer(({ hasError }: IProps): ReactElement => {
  const surveyService = useSurveyService();
  const styles = useStyles();
  const [age, setAge] = React.useState('');
  const { t } = useTranslation();

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>): void => {
    const ageSelected = event.target.value;
    setAge(ageSelected as string);
    surveyService.setAge(ageSelected as number);
  };

  const renderSelect = (): JSX.Element[] => {
    const selectItems = [];
    const endAge = 99;
    const ageIncrement = 9;

    for (let i = 0; i <= endAge; i += 10) {
      selectItems.push(
        <MenuItem key={i} value={i + ageIncrement}>
          {`${i} - ${i + ageIncrement} ${t('Years')}`}
        </MenuItem>
      );
    }

    return selectItems;
  };

  useEffect(() => {
    setAge(surveyService.age ? String(surveyService.age) : '');
  }, [surveyService]);

  return (
    <SurveyContainer header={`${t('AgeGroup')}?`}>
      <img className={styles.imageStyle} src={FMCalendar} alt="Calendar" />
      <FormControl className={styles.formControl}>
        <InputLabel id="demo-simple-select-label">{t('AgeGroup')}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={surveyService.age ? age : ''}
          onChange={handleChange}
        >
          {renderSelect()}
        </Select>
        {hasError && <FormHelperText>{t('ThisIsRequired')}</FormHelperText>}
      </FormControl>
    </SurveyContainer>
  );
});

export default AgeGroup;
