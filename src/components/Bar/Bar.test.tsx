import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Bar from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('<Bar />', () => {
  it('Should contain "outside" class if "value" smaller then 30% of the "maxValue"', () => {
    const wrapper = shallow(<Bar value={25} maxValue={100} label="test" labelColor="#fff" color="#fff" />);
    expect(wrapper.find('.outside')).toHaveLength(1);
  });

  it('Should NOT contain "outside" class if "value" greater or equal to 30% of the "maxValue"', () => {
    let wrapper = shallow(<Bar value={30} maxValue={100} label="test" labelColor="#fff" color="#fff" />);
    expect(wrapper.find('.outside')).toHaveLength(0);

    wrapper = shallow(<Bar value={35} maxValue={100} label="test" labelColor="#fff" color="#fff" />);
    expect(wrapper.find('.outside')).toHaveLength(0);
  });

  it('Should valueLabel callback instead of value if it was provided', () => {
    const wrapper = shallow(
      <Bar
        value={50}
        maxValue={100}
        label="test"
        labelColor="#fff"
        color="#fff"
        valueLabel={(value) => `test-${value}`}
      />
    );

    expect(wrapper.find('span').contains('test-50')).toBeTruthy();
  });

  it('Should correctly calculate ratio based on value and maxValue props', () => {
    let wrapper = mount(<Bar value={50} maxValue={100} label="test" labelColor="#fff" color="#fff" />);
    let style = wrapper.find('div > div').at(1).find('p').prop('style');
    expect(style?.width).toEqual('50%');

    wrapper = mount(<Bar value={30} maxValue={100} label="test" labelColor="#fff" color="#fff" />);
    style = wrapper.find('div > div').at(1).find('p').prop('style');
    expect(style?.width).toEqual('30%');

    wrapper = mount(<Bar value={25} maxValue={50} label="test" labelColor="#fff" color="#fff" />);
    style = wrapper.find('div > div').at(1).find('p').prop('style');
    expect(style?.width).toEqual('50%');

    wrapper = mount(<Bar value={15} maxValue={50} label="test" labelColor="#fff" color="#fff" />);
    style = wrapper.find('div > div').at(1).find('p').prop('style');
    expect(style?.width).toEqual('30%');

    wrapper = mount(<Bar value={15} maxValue={60} label="test" labelColor="#fff" color="#fff" />);
    style = wrapper.find('div > div').at(1).find('p').prop('style');
    expect(style?.width).toEqual('25%');
  });
});
