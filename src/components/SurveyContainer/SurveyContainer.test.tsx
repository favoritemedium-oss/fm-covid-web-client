import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SurveyContianer from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('<SurveyContianer />', () => {
  it('Should NOT have a header if it was NOT provided', () => {
    const wrapper = shallow(<SurveyContianer>Test</SurveyContianer>);
    expect(wrapper.find('h1')).toHaveLength(0);
  });

  it('Should have a header if it was provided', () => {
    const wrapper = shallow(<SurveyContianer header="test header">Test</SurveyContianer>);
    expect(wrapper.find('h1').contains('test header')).toBeTruthy();
  });
});
