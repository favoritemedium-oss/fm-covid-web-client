import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';

const oneSpacing = 1;

export const useStyles = makeStyles((theme: Theme) => createStyles({
  formControl: {
    margin: theme.spacing(oneSpacing),
    minWidth: 120
  },
  imageStyle: {
    maxWidth: '110px',
    display: 'block',
    margin: '0 auto'
  }
}));
