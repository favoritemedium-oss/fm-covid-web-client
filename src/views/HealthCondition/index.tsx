import React, { ReactElement } from 'react';
import Button from '@material-ui/core/Button';
import { useTranslation } from 'react-i18next';
import { useStyles } from './styles';
import { useSurveyService } from '../../services/SurveyService';
import { Preconditions } from '../../services/SurveyService/SurveyDao';
import SurveyContainer from '../../components/SurveyContainer';

export default function HealthCondition(): ReactElement {
  const surveyService = useSurveyService();
  const classes = useStyles();
  const preconditions = surveyService.preconditions ? surveyService.preconditions : [];
  const [conditions, setCondition] = React.useState(preconditions);
  const {
    None,
    Cancer,
    CardiovascularDisease,
    ChronicRespiratoryDisease,
    Diabetes,
    Hypertension
  } = Preconditions;
  const { t } = useTranslation();

  const handleChange = (name: Preconditions): void => {
    let selectedConditions: Preconditions[] = [];
    const allConditions = name === None ? [] : conditions;

    if (!conditions.includes(name)) {
      selectedConditions = [...allConditions, name];
    } else {
      selectedConditions = allConditions.filter((condition) => condition !== name);
    }
    setCondition(selectedConditions);
    surveyService.setPreconditions(selectedConditions);
  };

  const checkCondition = (condition: Preconditions): boolean => conditions.includes(condition);

  return (
    <SurveyContainer header={`${t('HealthConditions')}?`}>
      <div className={classes.buttonGroup}>
        <Button
          className={`${checkCondition(None) ? 'active' : ''}`}
          onClick={() => handleChange(None)}
        >
          {t('None')}
        </Button>
        <Button
          className={`${checkCondition(Cancer) ? 'active' : ''}`}
          disabled={checkCondition(None)}
          onClick={() => handleChange(Cancer)}
        >
          {t('Cancer')}
        </Button>
        <Button
          className={`${checkCondition(CardiovascularDisease) ? 'active' : ''}`}
          disabled={checkCondition(None)}
          onClick={() => handleChange(CardiovascularDisease)}
        >
          {t('CardiovascularDisease')}
        </Button>
        <Button
          className={`${checkCondition(ChronicRespiratoryDisease) ? 'active' : ''}`}
          disabled={checkCondition(None)}
          onClick={() => handleChange(ChronicRespiratoryDisease)}
        >
          {t('ChronicRespiratoryDisease')}
        </Button>
        <Button
          className={`${checkCondition(Diabetes) ? 'active' : ''}`}
          disabled={checkCondition(None)}
          onClick={() => handleChange(Diabetes)}
        >
          {t('Diabetes')}
        </Button>
        <Button
          className={`${checkCondition(Hypertension) ? 'active' : ''}`}
          disabled={checkCondition(None)}
          onClick={() => handleChange(Hypertension)}
        >
          {t('Hypertension')}
        </Button>
      </div>
    </SurveyContainer>
  );
}
