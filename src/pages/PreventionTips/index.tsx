import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import FMBubbleSpeech from '../../assets/images/favorite_medium_bubble_speech.svg';
import PageContainer from '../../components/PageContainer';

import { useStyles } from './styles';

interface ITips {
  title: string;
  links: string[];
}

function PreventionTips(): ReactElement {
  const styles = useStyles();
  const { t } = useTranslation();
  const preventionTips: ITips[] = t('preventionTipsLinks', { returnObjects: true });

  const renderLinks = (links: string[]): ReactElement[] => links.map((link) => (
    <li key={link} dangerouslySetInnerHTML={{ __html: link }} />
  ));

  const renderTips = (): ReactElement[] => preventionTips.map((tips) => (
    <div key={tips.title}>
      <p className="title">{tips.title}</p>
      <ul>{renderLinks(tips.links)}</ul>
    </div>
  ));

  return (
    <PageContainer headerIcon={<img src={FMBubbleSpeech} alt="Bubble Speech" />} header={t('PreventionTipsTitle')}>
      <p className={styles.subHeader}>
        {t('TipsParagraph')}
      </p>
      <div className={styles.linkArea}>
        {renderTips()}
      </div>
    </PageContainer>
  );
}

export default PreventionTips;
