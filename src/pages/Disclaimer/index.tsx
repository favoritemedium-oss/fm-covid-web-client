import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import PageContainer from '../../components/PageContainer';
import Typography from '../../components/Typography';

function Disclaimer(): ReactElement {
  const { t } = useTranslation();

  return (
    <PageContainer header={t('DisclaimerTitle')}>
      <Typography>
        <p>{t('Disclaimer')}</p>
      </Typography>
    </PageContainer>
  );
}

export default Disclaimer;
