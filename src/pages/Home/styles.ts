import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
  innerStyle: {
    textAlign: 'center'
  },

  imageStyle: {
    margin: '0 auto',
    maxWidth: '350px',
    marginBottom: '30px'
  },

  header: {
    fontFamily: 'Montserrat, sans-serif',
    margin: 0,
    fontSize: '22px',
    fontWeight: 'normal',
    color: '#373C58',

    [theme.breakpoints.up('md')]: {
      fontSize: '32px',
      maxWidth: '650px'
    }
  },

  paragraph: {
    fontSize: '14px',
    fontFamily: 'Open Sans, sans-serif',
    fontWeight: 'normal',
    color: 'rgba(102, 102, 102, 0.87)',

    [theme.breakpoints.up('md')]: {
      fontSize: '16px',
      maxWidth: '650px'
    }
  }
}));
