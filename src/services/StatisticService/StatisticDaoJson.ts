import dayjs from 'dayjs';
import axios from 'axios';
import { IStatistic, IStatisticDao } from './StatisticDao';
import { Preconditions } from '../SurveyService/SurveyDao';

interface IAgeGroupStat {
  cfr_known: number;
  patient_count_known: number;
  patient_count_estimated: number;
  cfr_estimated: number;
  total_population: number;
  percentage_infected: number;
  mortality_per_100k: number;
}

interface IMortalityStatsPer100k {
  [name: string]: number
}

interface IAgeBreakdown {
  [name: string]: {
    male: IAgeGroupStat | null,
    female: IAgeGroupStat | null,
    mortality_stats_per_100k: IMortalityStatsPer100k,
  };
}

interface IComorbidityResponse {
  chronic_obstructive_lung_disease: number;
  coronary_heart_disease: number;
  diabetes: number;
  hypertension: number;
  current_smoker: number;
}

interface IJsonStatistic {
  age_sex_breakdown: IAgeBreakdown;
  default_cfr: number;
  comorbidity: IComorbidityResponse;
}

const BASE_URL = 'https://covid9stats.storage.googleapis.com/covid9stats';

// Dao implementation which will load statistic as plain json
export default class StatisticDaoLocal implements IStatisticDao {
  public async loadStatistic(): Promise<IStatistic | null> {
    const fileName = `${dayjs().utc().format('YYYY_MM_DD')}_data.json`;
    let json;

    try {
      json = (await axios.get<IJsonStatistic>(`${BASE_URL}/${fileName}`)).data;
    } catch {
      json = (await axios.get<IJsonStatistic>(`${BASE_URL}/latest_data.json`)).data;
    }

    const statistic: IStatistic = {
      groupsStatistics: [],
      statisticByAge: []
    };

    const ageGroups = Object.keys(json.age_sex_breakdown);
    for (const ageGroup of ageGroups) {
      const rangeArr = ageGroup.split('-');
      if (!Array.isArray(rangeArr) || rangeArr.length !== 2) {
        console.error(`Unknown age group format ("${ageGroup}")`);
        return null;
      }

      const start = Number(rangeArr[0]);
      const end = Number(rangeArr[1]);

      let maleStat = json.age_sex_breakdown[ageGroup].male;
      let femaleStat = json.age_sex_breakdown[ageGroup].female;
      const mortalityStats = json.age_sex_breakdown[ageGroup].mortality_stats_per_100k;
      const comorbidity = [
        { name: Preconditions.Cancer, value: json.comorbidity.current_smoker },
        { name: Preconditions.CardiovascularDisease, value: json.comorbidity.coronary_heart_disease },
        { name: Preconditions.ChronicRespiratoryDisease, value: json.comorbidity.chronic_obstructive_lung_disease },
        { name: Preconditions.Diabetes, value: json.comorbidity.diabetes },
        { name: Preconditions.Hypertension, value: json.comorbidity.hypertension }
      ];

      if (!maleStat) {
        maleStat = femaleStat;
      }

      if (!femaleStat) {
        femaleStat = maleStat;
      }

      if (!maleStat || !femaleStat) {
        console.error(`Both, male and female stats for age group "${ageGroups}" cannot be null`);
        return null;
      }

      statistic.groupsStatistics.push({
        cfrEstimated: maleStat.cfr_estimated,
        cfrKnown: maleStat.cfr_known,
        patientCountEstimated: maleStat.patient_count_estimated,
        patientCountKnown: maleStat.patient_count_known,
        percentageInfected: maleStat.percentage_infected,
        totalPopulation: maleStat.total_population,
        mortalityPer100k: maleStat.mortality_per_100k
      });

      statistic.groupsStatistics.push({
        cfrEstimated: femaleStat.cfr_estimated,
        cfrKnown: femaleStat.cfr_known,
        patientCountEstimated: femaleStat.patient_count_estimated,
        patientCountKnown: femaleStat.patient_count_known,
        percentageInfected: femaleStat.percentage_infected,
        totalPopulation: femaleStat.total_population,
        mortalityPer100k: femaleStat.mortality_per_100k
      });

      const maleIndex = statistic.groupsStatistics.length - 2;
      const femaleIndex = statistic.groupsStatistics.length - 1;
      for (let i = start; i <= end; i += 1) {
        statistic.statisticByAge.push({
          male: statistic.groupsStatistics[maleIndex],
          female: statistic.groupsStatistics[femaleIndex],
          mortalityStats,
          comorbidity
        });
      }
    }

    return statistic;
  }
}
