import React, { ReactElement, ReactNode } from 'react';
import Container from '@material-ui/core/Container';
import clsx from 'clsx';

import { useStyles } from './styles';

interface IProps {
  header?: string;
  headerIcon?: ReactNode;
  className?: string;
  children: ReactNode;
}

function PageContainer(props: IProps): ReactElement {
  const {
    header, headerIcon, children, className
  } = props;

  const styles = useStyles();
  const cls = clsx(styles.containerStyle, className);

  const renderHeader = (): ReactElement|null => {
    if (!header && !headerIcon) {
      return null;
    }

    return (
      <p className={styles.header}>
        {headerIcon}
        {header}
      </p>
    );
  };

  return (
    <Container className={cls}>
      <div className={styles.innerContent}>
        {renderHeader()}
        {children}
      </div>
    </Container>
  );
}

export default PageContainer;
