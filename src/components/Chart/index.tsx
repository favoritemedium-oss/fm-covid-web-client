import React, { ReactElement } from 'react';

import { useStyles } from './styles';

interface IProps {
  title: string;
  subtitle: string;
  count: number;
  renderBar: (index: number) => ReactElement;
}

export default function Chart(props: IProps): ReactElement {
  const {
    title,
    subtitle,
    count,
    renderBar
  } = props;

  const styles = useStyles();
  const bars: ReactElement[] = [];

  for (let i = 0; i < count; i += 1) {
    bars.push(renderBar(i));
  }

  return (
    <div className={styles.chart}>
      <p className={styles.title}>{title}</p>
      <p className={styles.subtitle}>{subtitle}</p>
      <div className={styles.chartArea}>{bars}</div>
    </div>
  );
}
