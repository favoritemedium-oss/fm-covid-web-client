import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
  containerStyle: {
    paddingTop: '64px',
    textAlign: 'center',
    maxWidth: '990px',
    margin: '0 auto',
    backgroundColor: '#fff',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,

    [theme.breakpoints.up('md')]: {
      borderRadius: '20px',
      height: 'auto',
      padding: '0',
      marginTop: '110px'
    }
  },

  innerContent: {
    textAlign: 'left',

    [theme.breakpoints.up('md')]: {
      maxWidth: '550px',
      padding: '20px 0',
      width: '100%',
      margin: '0 auto'
    }
  },

  header: {
    fontSize: '26px',
    fontFamily: 'Montserrat, sans-serif',
    color: '#373C58',

    '& img': {
      width: '40px',
      verticalAlign: 'middle',
      marginRight: '15px'
    }
  }
}));
