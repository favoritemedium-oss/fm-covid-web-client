import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PageContianer from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('<PageContianer />', () => {
  it('Should NOT have a header if it was NOT provided', () => {
    const wrapper = shallow(<PageContianer>Test</PageContianer>);
    expect(wrapper.find('p')).toHaveLength(0);
  });

  it('Should have a header if header or headerIcon props were provided', () => {
    let wrapper = shallow(<PageContianer header="test header">Test</PageContianer>);
    expect(wrapper.find('p').hasClass(/header/i)).toBeTruthy();

    wrapper = shallow(<PageContianer headerIcon="test header">Test</PageContianer>);
    expect(wrapper.find('p').hasClass(/header/i)).toBeTruthy();
  });

  it('Should have an icon element if the relevant prop was provided', () => {
    const wrapper = shallow(<PageContianer headerIcon={<div className="icon" />}>Test</PageContianer>);
    expect(wrapper.find('.icon')).toHaveLength(1);
  });
});
