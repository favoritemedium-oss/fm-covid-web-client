import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => createStyles({
  genderSelection: {
    marginTop: '20px',
    display: 'flex',
    flexDirection: 'row',

    '& button': {
      width: '100%'
    },

    '& > div': {
      flexGrow: '1',
      flexBasis: '0',
      background: '#FFFFFF',
      border: '4px solid #fff',
      boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
      borderRadius: '20px',
      overflow: 'hidden',
      padding: '0'
    },

    '& > div:first-child': {
      marginRight: '7px'
    },

    '& > div:last-child': {
      marginLeft: '7px'
    },

    '& .MuiFormControlLabel-root': {
      display: 'block'
    },

    '& .active': {
      border: '4px solid #68B881'
    }
  },

  buttonContent: {
    padding: 10,

    '& img': {
      width: '50px',
      display: 'block',
      margin: '0 auto'
    },

    '& p': {
      paddingTop: 5,
      margin: '0',
      textTransform: 'none',
      fontFamily: 'Montserrat, sans-serif',
      fontSize: '16px'
    }
  }
}));
