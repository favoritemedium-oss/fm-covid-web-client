import { Context, ReactNode, useContext } from 'react';

export interface IServiceContextProps<T> {
  children: ReactNode;
  dao: T;
}

export default class Service<T> {
  protected dao: T;

  constructor(dao: T) {
    this.dao = dao;
  }
}

export function createServiceHook<S extends Service<D>, D>(context: Context<S|null>): () => S {
  return (): S => {
    const service = useContext(context);

    if (!service) {
      // Required by TypeScript so we don't need to check for null all the time
      throw new Error('useStore must be used within a Store context.');
    }

    return service;
  };
}
