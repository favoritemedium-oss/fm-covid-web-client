import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => createStyles({
  button: {
    border: '1px solid #373C58',
    flexGrow: 1,
    padding: '5px 45px',
    fontSize: '14px',
    maxWidth: '200px',
    width: '100%',
    margin: '20px 15px 0 15px',
    borderRadius: '5px'
  },

  primary: {
    color: '#fff',
    textTransform: 'none',
    fontWeight: 'normal',
    backgroundColor: '#373C58',
    fontFamily: 'Open Sans, sans-serif'
  }
}));
