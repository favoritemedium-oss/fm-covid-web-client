import React, { ReactElement } from 'react';
import PageContainer from '../../components/PageContainer';

function NotFound(): ReactElement {
  return (
    <PageContainer>
      <p>404 Not Found</p>
    </PageContainer>
  );
}

export default NotFound;
