import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
  buttonGroup: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',

    '& button': {
      margin: '10px 5px',
      flex: '0 46%',
      minHeight: '90px',
      background: '#fff',
      border: '1px solid #E7E5E5',
      boxSizing: 'border-box',
      boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
      borderRadius: '20px',
      fontSize: '14px',
      textTransform: 'none',
      color: '#E6447E',

      [theme.breakpoints.up('md')]: {
        flex: '0 31%'
      }
    },

    '& .active': {
      border: '3px solid #68B881'
    }
  }
}));
