import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => createStyles({
  content: {
    alignItems: 'stretch',
    justifyContent: 'flex-start',

    '& .MuiStepIcon-root': {
      color: '#D8D8D8'
    },

    '& .MuiStepIcon-root.MuiStepIcon-completed': {
      color: '#68B881'
    },

    '& .MuiStepIcon-root.MuiStepIcon-active': {
      color: '#373C58'
    },

    '& .MuiStepIcon-text': {
      fill: '#fff'
    },

    '& .MuiStepLabel-labelContainer': {
      display: 'none'
    }
  },

  heading: {
    fontFamily: 'Montserrat, sans-serif',
    fontSize: '26px',
    textAlign: 'center'
  },

  buttonGroup: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  }
}));
