import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Chart from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('<Char />', () => {
  it('Should render exact number of children based on "count" property', () => {
    const wrapper = shallow(
      <Chart
        title="test"
        subtitle="subtitle"
        count={5}
        renderBar={(index) => <div key={index} className="bar" />}
      />
    );

    expect(wrapper.find('.bar')).toHaveLength(5);
  });
});
