import React, { ReactElement, CSSProperties } from 'react';
import clsx from 'clsx';

import { useStyles } from './styles';

interface IProps {
  label: string;
  value: number;
  maxValue: number;
  color: string;
  labelColor: string;
  valueLabel?: (value: number) => string;
}

export default function Bar(props: IProps): ReactElement {
  const {
    label,
    value,
    color,
    labelColor,
    maxValue,
    valueLabel
  } = props;

  const styles = useStyles();

  const barStyle = (): CSSProperties => {
    const total = Math.ceil(maxValue);
    const width = (value / total) * 100;
    const pad = value === 0 ? 0 : 5;

    return {
      width: `${width}%`,
      paddingRight: `${pad}px`,
      backgroundColor: color
    };
  };

  const isLabelOutside = (): boolean => {
    const total = Math.ceil(maxValue);
    const percent = (value / total) * 100;
    return percent < 30 && value !== 0;
  };

  const barLabel = valueLabel ? valueLabel(value) : value;

  return (
    <div key={label} className={styles.row}>
      <div className={styles.label}>
        <p style={{ color: labelColor }}>
          <span>{label}</span>
        </p>
      </div>
      <div className={styles.bar}>
        <p style={barStyle()}>
          <span className={clsx({ outside: isLabelOutside() })}>{barLabel}</span>
        </p>
      </div>
    </div>
  );
}
