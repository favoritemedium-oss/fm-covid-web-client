import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import FMCovid from '../../assets/images/favorite_medium_covid.svg';
import PageContainer from '../../components/PageContainer';
import Typography from '../../components/Typography';

interface IAboutData {
  paragraph: string;
}

function AboutData(): ReactElement {
  const { t } = useTranslation();
  const aboutData: IAboutData[] = t('AboutParagraphs', { returnObjects: true });
  const otherData: IAboutData[] = t('DataSources', { returnObjects: true });

  const renderData = (dataList: IAboutData[]): ReactElement[] => dataList.map((data) => (
    <p key={data.paragraph} dangerouslySetInnerHTML={{ __html: data.paragraph }} />
  ));

  return (
    <PageContainer headerIcon={<img src={FMCovid} alt="Virus Icon" />} header={t('AboutData')}>
      <Typography>
        <div>
          {renderData(aboutData)}
        </div>
        <div className="other-sources">
          <p className="label">{t('OtherDataSources')}</p>
          {renderData(otherData)}
        </div>
      </Typography>
    </PageContainer>
  );
}

export default AboutData;
