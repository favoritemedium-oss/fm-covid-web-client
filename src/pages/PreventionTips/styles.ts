import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => createStyles({
  subHeader: {
    fontFamily: 'Open Sans, sans-serif',
    color: '#373C58'
  },

  linkArea: {
    fontFamily: 'Open Sans, sans-serif',
    marginTop: '30px',

    '& > div': {
      marginBottom: '25px'
    },

    '& .title': {
      fontWeight: 'bold',
      color: '#373C58'
    },

    '& ul': {
      padding: '0 20px',

      '& li': {
        paddingBottom: '10px',

        '& a': {
          color: '#68B881',
          fontWeight: 'bold',
          textDecoration: 'none'
        }
      }
    }
  }
}));
