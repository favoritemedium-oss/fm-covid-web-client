
import React, { useState } from 'react';
import { observable, action, runInAction } from 'mobx';
import { ISurveyDao, Gender, Preconditions } from './SurveyDao';
import SurveyDaoLocal from './SurveyDaoLocal';
import Service, { IServiceContextProps, createServiceHook } from '../Service';

class SurveyService extends Service<ISurveyDao> {
  @observable public pending = false;

  @observable public age: number|null = null;

  @observable public gender: Gender|null = null;

  @observable public preconditions: Preconditions[]|null = null;

  @action public async load(): Promise<void> {
    const stats = await this.dao.loadUserStats();
    if (stats) {
      this.age = stats.age;
      this.gender = stats.gender;
      this.preconditions = stats.preconditions;
    }
  }

  @action public async setAge(age: number): Promise<void> {
    await this.dao.setAge(age);
    runInAction(() => {
      this.age = age;
    });
  }

  @action public async setGender(gender: Gender): Promise<void> {
    await this.dao.setGender(gender);
    runInAction(() => {
      this.gender = gender;
    });
  }

  @action public async setPreconditions(preconditions: Preconditions[]): Promise<void> {
    await this.dao.setPreconditions(preconditions);
    runInAction(() => {
      this.preconditions = preconditions;
    });
  }

  @action public async resetData(): Promise<void> {
    this.age = null;
    this.gender = null;
    this.preconditions = null;
    await this.dao.resetData();
  }
}

// Create Context and service hooks
const SurveyServiceContext = React.createContext<SurveyService|null>(null);
export const SurveyServiceProvider = ({ dao, children }: IServiceContextProps<ISurveyDao>): React.ReactElement => {
  const [service] = useState(new SurveyService(dao));
  return <SurveyServiceContext.Provider value={service}>{children}</SurveyServiceContext.Provider>;
};

SurveyServiceProvider.defaultProps = {
  dao: new SurveyDaoLocal()
};

export const useSurveyService = createServiceHook<SurveyService, ISurveyDao>(SurveyServiceContext);
