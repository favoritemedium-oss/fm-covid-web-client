import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => createStyles({
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  },

  label: {
    flex: '0 0 70px',

    '& p': {
      fontSize: '10px',
      height: '100%',
      margin: '0',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      fontFamily: 'Montserrat, sans-serif'
    }
  },

  bar: {
    flex: 1,
    borderLeft: '1px solid #C4C4C4',

    '& span': {
      minWidth: '50px',
      display: 'inline-block',
      fontWeight: 'bold'
    },

    '& span.outside': {
      position: 'absolute',
      right: '-30px'
    },

    '& p': {
      padding: '10px 0',
      backgroundColor: 'green',
      alignItems: 'center',
      justifyContent: 'center',
      height: '33px',
      textAlign: 'right',
      paddingRight: '5px',
      position: 'relative'
    }
  }
}));
