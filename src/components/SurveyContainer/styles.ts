import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => createStyles({
  containerStyle: {
    textAlign: 'center',
    fontFamily: 'Montserrat, sans-serif',
    paddingTop: '0',
    marginTop: '0'
  },

  header: {
    fontSize: '26px',
    fontWeight: 'normal'
  }
}));
