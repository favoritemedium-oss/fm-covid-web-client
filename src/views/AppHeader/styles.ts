import { makeStyles } from '@material-ui/styles';
import { createStyles, createMuiTheme, Theme } from '@material-ui/core';

const pinkColor = '#E6447E';
export const useStyles = makeStyles((theme: Theme) => createStyles({
  hamburger: {
    color: pinkColor,
    zIndex: 1,

    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },

  typography: {
    position: 'absolute',
    width: '100%',
    textAlign: 'center',
    left: '0',
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: 'bold',
    textDecoration: 'none',

    [theme.breakpoints.up('md')]: {
      position: 'relative',
      textAlign: 'left'
    }
  },

  '& .MuiDrawer-paper-174': {
    color: pinkColor
  },

  list: {
    width: 250
  },

  menuItem: {
    height: '50px',
    color: pinkColor,
    fontFamily: 'Montserrat, sans-serif'
  },

  fullList: {
    width: 'auto'
  },

  spacer: {
    flexGrow: 1
  },

  sectionDesktop: {
    color: pinkColor,
    display: 'none',
    '& .MuiMenuItem-root-104': {
      fontFamily: 'Montserrat, sans-serif'
    },

    [theme.breakpoints.up('md')]: {
      display: 'flex'
    }
  },

  select: {
    width: 'auto',

    '& >div': {
      color: pinkColor,
      minWidth: '70px',
      fontFamily: 'Montserrat, sans-serif',

      '& li': {
        color: pinkColor
      }
    },

    '& svg': {
      color: pinkColor
    },

    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        border: 'none'
      },

      '&:hover fieldset': {
        border: 'none'
      },

      '&.Mui-focused fieldset': {
        border: 'none'
      }
    },

    '& fieldset': {
      border: 'none'
    },

    '& .MuiSelect-selectMenu-127': {
      textOverflow: 'unset',
      fontFamily: 'Montserrat, sans-serif'
    },

    [theme.breakpoints.up('md')]: {
      width: '115px'
    }
  }
}));

export const headerTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#ffffff'
    }
  }
});
